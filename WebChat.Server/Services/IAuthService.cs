﻿using WebChat.Server.Models.Api;
using WebChat.Server.Models.SqlDbEntities;

namespace WebChat.Server.Services
{
    public interface IAuthService
    {
        AuthResponse Authenticate(SignInCredentials credentials);
        void DeleteExpiredTokens();
        void Logout(string refreshToken);
        AppUser Register(RegistrationCredentials credentials);
        AuthResponse RenewToken(string refreshToken);
    }
}