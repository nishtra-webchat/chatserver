﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebChat.Server.DataAccess;
using WebChat.Server.Models.SqlDbEntities;

namespace WebChat.Server.Services
{
    public class DbDataInitializer
    {
        private readonly IServiceScopeFactory scopeFactory;

        public DbDataInitializer(IServiceScopeFactory scopeFactory)
        {
            this.scopeFactory = scopeFactory;
        }

        public void Initialize()
        {
            using (var serviceScope = scopeFactory.CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<AppDbContext>())
                {
                    context.Database.Migrate();
                }
            }
        }

        public void SeedData()
        {
            using (var serviceScope = scopeFactory.CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<AppDbContext>())
                {
                    if (!context.Users.Any())
                    {
                        //add users
                        var users = new List<AppUser>() {
                            new AppUser() {
                                Email = "admin@mail.com",
                                Username = "admin",
                                Password = "admin"
                            },
                            new AppUser {
                                Email = "jane@gmail.com",
                                Username = "Jane",
                                Password = "123"
                            },
                            new AppUser {
                                Email = "mr.robot@gmail.com",
                                Username = "I_AM_ROBOT",
                                Password = "123"
                            }
                        };
                        context.Users.AddRange(users);
                        context.SaveChanges();

                        var conversations = new List<Conversation>() {
                            Conversation.CreateGroupConversation(users[0], "Test convo 1"),
                            Conversation.CreateGroupConversation(users[0], "C# Discussions"),
                            Conversation.CreateGroupConversation(users[1], "Jane's personal group"),
                            Conversation.CreateGroupConversation(users[0], "Admin notes"),
                            Conversation.CreatePrivateConversation(users[0], users[1])
                        };
                        context.Conversations.AddRange(conversations);
                        context.SaveChanges();

                        var messages = new List<Message>() {
                            new Message("Sup", users[0].Id, conversations[0].Id),
                            new Message("Hows it going?", users[0].Id, conversations[0].Id),
                            new Message("Sup", users[1].Id, conversations[0].Id),
                        };
                        context.Messages.AddRange(messages);
                        context.SaveChanges();
                    }
                }
            }
        }
    }

}
