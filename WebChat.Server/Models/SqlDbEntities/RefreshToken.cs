﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebChat.Server.Models.SqlDbEntities
{
    public class RefreshToken
    {
        [Key]
        public string Token { get; set; }
        public DateTime TimeCreated { get; set; }

        public int UserId { get; set; }
        public virtual AppUser User { get; set; }
    }
}
