﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebChat.Server.Models.Api
{
    public class ErrorResponse
    {
        public string Error { get; set; }

        public ErrorResponse()
        {
        }

        public ErrorResponse(string error)
        {
            Error = error;
        }
    }
}
