﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebChat.Server.Models.SqlDbEntities;

namespace WebChat.Server.Models.Api
{
    public class ConversationSearchItem
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public bool IsUser { get; set; }

        public ConversationSearchItem()
        {
        }

        public ConversationSearchItem(Conversation conversation)
        {
            Name = conversation.Name;
            Id = conversation.Id;
            IsUser = false;
        }

        public ConversationSearchItem(AppUser user)
        {
            Name = user.Username;
            Id = user.Id;
            IsUser = true;
        }
    }
}
