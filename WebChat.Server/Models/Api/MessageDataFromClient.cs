﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebChat.Server.Models.Api
{
    /// <summary>
    /// Data structure describing all the necessary information about a chat message coming from the client.
    /// </summary>
    public class MessageDataFromClient
    {
        /// <summary>
        /// Database ID for Conversation. Used in all cases except for a new private chat.
        /// </summary>
        public int? ReceiverConversationId { get; set; }
        /// <summary>
        /// Database ID for User recepient. Used in all private chats.
        /// </summary>
        public int? ReceiverUserId { get; set; }
        /// <summary>
        /// Text content of the message
        /// </summary>
        public string MessageText { get; set; }
    }
}
