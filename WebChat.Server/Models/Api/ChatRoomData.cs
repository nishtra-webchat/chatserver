﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebChat.Server.Models.Api
{
    public class ChatRoomData
    {
        public IEnumerable<MessageDataToClient> LatestMessages { get; set; }
        public IEnumerable<ConversationMember> Members { get; set; }
    }
}
